import com.rometools.rome.feed.synd.*;
import com.rometools.rome.io.SyndFeedOutput;

import java.io.FileWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class FeedWriter implements IUmrechnen {
    List<SyndEntry> entries = new ArrayList<>();

    private final SyndEntry entry;
    private final SyndContent content;
    private final SyndFeed feed;

    public FeedWriter() {
        entry = new SyndEntryImpl();
        content = new SyndContentImpl();
        feed = new SyndFeedImpl();
    }

    public void createFeed(String title, String origCurrency, String targetCurrency, double betrag) {
        feed.setFeedType("atom_0.3");

        entry.setTitle(title);
        entry.setAuthor("ArZi");
        entry.setPublishedDate(new Date());
        content.setType(targetCurrency);
        content.setValue("Umrechnung von "+ origCurrency + "  nach " + targetCurrency+", " + betrag + origCurrency + " sind " + umrechnen(targetCurrency, betrag) + targetCurrency);
        entry.setDescription(content);
        entries.add(entry);

        feed.setEntries(entries);

        String fileName = "log";
        try {
            Writer writer = new FileWriter(fileName);
            SyndFeedOutput output = new SyndFeedOutput();
            output.output(feed, writer);
            writer.close();
        } catch (Exception e) {
            System.out.println("Error occured: " + e);
        }
    }


    @Override
    public double umrechnen(String var, double betrag) {
        if(var.equals("CHF")) {
            return betrag*1.029;
        } else if (var.equals("Dollar")) {
            return betrag*1.220;
        } else {
            System.out.println("Ungültiges Format");
            return 0.0;
        }
    }
}
