import com.rometools.rome.io.FeedException;

import java.io.IOException;
import java.text.ParseException;

public class App {
    //private static final DateFormat DATE_PARSER = new SimpleDateFormat("yyyy-MM-dd");
    public static void main(String[] args) throws ParseException, IOException, FeedException {
        FeedWriter writer = new FeedWriter();
        writer.createFeed("Umrechnung nach EurToCHF", "EUR", "CHF", 100);
    }
}
