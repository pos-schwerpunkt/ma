public abstract class WR implements IUmrechnen {
    protected WR nachfolger;

    public WR(WR nachfolger) {
        this.nachfolger = nachfolger;
    }


    public abstract boolean responsible(String var);
    public abstract double berechnen(double betrag);

    @Override
    public double umrechnen(String var, double betrag) throws CurrencyNotFoundException {
        if(responsible(var)) {
            return berechnen(betrag);
        } else {
            if(this.nachfolger != null) {
                System.out.println(getClass().getCanonicalName() + " is not responsible --> moving on to next chain");
                return this.nachfolger.umrechnen(var, betrag);
            } else {
                throw new CurrencyNotFoundException();
            }
        }
    }
}
