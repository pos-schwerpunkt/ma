public class DecoratedWR implements IUmrechnen {

    private final IUmrechnen decorator;
    protected WR nachfolger;

    public DecoratedWR(IUmrechnen decorator, WR nachfolger) {
        this.decorator = decorator;
        this.nachfolger = nachfolger;
    }

    @Override
    public double umrechnen(String var, double betrag) throws CurrencyNotFoundException {
        double gebuehr = (betrag * 1.05) * 0.005;
        return decorator.umrechnen(var, betrag) + gebuehr;
    }
}
