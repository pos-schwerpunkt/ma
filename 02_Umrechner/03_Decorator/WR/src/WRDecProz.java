public class WRDecProz implements IUmrechnen {
    protected WR decorated;
    protected WR nachfolger;

    public WRDecProz(WR decorated, WR nachfolger) {
        this.decorated = decorated;
        this.nachfolger = nachfolger;
    }

    @Override
    public double umrechnen(String var, double betrag) throws CurrencyNotFoundException {
        return decorated.umrechnen(var, betrag*1.005);
    }
}
