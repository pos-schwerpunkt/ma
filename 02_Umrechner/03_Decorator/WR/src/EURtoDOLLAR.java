public class EURtoDOLLAR extends WR {

    public EURtoDOLLAR(WR nachfolger) {
        super(nachfolger);
    }

    @Override
    public boolean responsible(String var) {
        return var.equals("EURtoDOLLAR");
    }

    @Override
    public double berechnen(double betrag) {
        return betrag*0.93;
    }


}
