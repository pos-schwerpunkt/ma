public class WRDecEur implements IUmrechnen{
    protected WR decorated;
    protected WR nachfolger;

    public WRDecEur(WR decorated, WR nachfolger) {
        this.decorated = decorated;
        this.nachfolger = nachfolger;
    }

    @Override
    public double umrechnen(String var, double betrag) throws CurrencyNotFoundException {
        if(var.startsWith("EURto")) {
            betrag+=5;
            return decorated.umrechnen(var, betrag);
        } else {
            return umrechnen(var,betrag);
        }
    }
}
