public class App {
    public static void main(String[] args) throws CurrencyNotFoundException {
        WR kettenbeginn = new EURtoDOLLAR(new EURtoYEN(new EURtoCHF(null)));
        WRDecProz decorated = new WRDecProz(kettenbeginn, new EURtoCHF(new EURtoYEN(new EURtoDOLLAR(null))));
        WRDecEur decEur = new WRDecEur(kettenbeginn, new EURtoCHF(new EURtoYEN(new EURtoDOLLAR(null))));

        System.out.println("Keine Gebühren: " + kettenbeginn.umrechnen("EURtoCHF", 1500));
        System.out.println("Decorator: 0,5% Gebühr: " + decorated.umrechnen("EURtoCHF", 1500) + "\n");
        System.out.println("Decorator: Gebühr auf EURO-Wechsel: " + decEur.umrechnen("EURtoCHF", 1500) + "\n");
    }
}
