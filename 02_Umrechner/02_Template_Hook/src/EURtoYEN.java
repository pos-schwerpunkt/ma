public class EURtoYEN extends WR {
    public EURtoYEN(WR nachfolger) {
        super(nachfolger);
    }

    @Override
    public boolean responsible(String var) {
        return var.equals("EURtoYEN");
    }

    @Override
    public double berechnen(double betrag) {
        return betrag*10000;
    }


}
