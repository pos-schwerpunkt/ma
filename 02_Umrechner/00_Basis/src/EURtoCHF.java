public class EURtoCHF extends WR {
    public EURtoCHF(WR nachfolger) {
        super(nachfolger);
    }

    @Override
    public double umrechnen(String var, double betrag) throws CurrencyNotFoundException {
        if (var.equals("EURtoCHF")) {
            return betrag * 1.07;
        } else {
            if (this.nachfolger != null) {
                System.out.println("Not responsible -> moving on to next chain member");
                return this.nachfolger.umrechnen(var, betrag);
            } else {
                throw new CurrencyNotFoundException();
            }
        }
    }
}
