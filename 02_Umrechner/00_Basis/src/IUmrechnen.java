public interface IUmrechnen {
    double umrechnen(String var, double betrag) throws CurrencyNotFoundException;
}
