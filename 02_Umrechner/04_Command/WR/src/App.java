public class App {
    public static void main(String[] args) {
        Umrechner ch = new Umrechner(1.022);
        Umrechner us = new Umrechner(1.234);
        EURtoCHF chf = new EURtoCHF(ch, "CHF", 1000);
        EURtoCHF chf2 = new EURtoCHF(ch, "CHF", 2000);
        Umrechner umrechner = new Umrechner();
        EURtoDOLLAR dollar = new EURtoDOLLAR(us, "Dollar", 1000);
        EURtoDOLLAR dollar2 = new EURtoDOLLAR(us, "Dollar", 2000);
        umrechner.addCalc(chf);
        umrechner.addCalc(chf2);
        umrechner.addCalc(dollar);
        umrechner.addCalc(dollar2);

        umrechner.execute();

        System.out.println("History:");
        umrechner.showHistory();

        System.out.println("Recalculating the second entry:");
        umrechner.reCalc(2);

        System.out.println("History:");
        umrechner.showHistory();

        System.out.println("Recalculation last entry and deleting it afterwards:");
        umrechner.calcAndDelete();

        System.out.println("History:");
        umrechner.showHistory();

        umrechner.replayDESC();
    }
}
