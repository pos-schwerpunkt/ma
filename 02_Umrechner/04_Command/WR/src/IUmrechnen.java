public interface IUmrechnen {
    double umrechnen(double betrag);
    double getBetrag();
    String getCurrency();
}
