public class CommandPatternDemo {
    public static void main(String[] args) {
        Stock abc = new Stock();
        Stock custom = new Stock("MyStock", 12);

        BuyStock buyStockOrder = new BuyStock(abc);
        SellStock sellStockOrder = new SellStock(abc);
        BuyStock customBuyStock = new BuyStock(custom);
        SellStock customSellStock = new SellStock(custom);

        Broker broker = new Broker();
        broker.takeOrder(buyStockOrder);
        broker.takeOrder(sellStockOrder);
        broker.takeOrder(buyStockOrder);
        broker.takeOrder(customBuyStock);
        broker.takeOrder(customSellStock);
        broker.getOrderCount();

        broker.placeOrders();
    }
}
