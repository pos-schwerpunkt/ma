import java.util.ArrayList;
import java.util.List;

public class Broker {
    private List<Order> orderList = new ArrayList<>();

    public void takeOrder(Order order) {
        orderList.add(order);
    }

    public void getOrderCount() {
        System.out.println(orderList.size());
    }

    public void placeOrders() {
        for(Order o : orderList) {
            o.execute();
        }
        orderList.clear();
    }
}
