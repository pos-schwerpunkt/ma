public class WR {
    private CURRENCY currency;
    private double betrag;
    private double fee;

    public WR(CURRENCY currency, double betrag, double fee) {
        super();
        this.currency = currency;
        this.betrag = betrag;
        this.fee = fee;
    }

    @Override
    public String toString() {
        return "Currency=" + currency + ", Betrag=" + betrag +
                ", optional fee=" + fee;
    }
}
