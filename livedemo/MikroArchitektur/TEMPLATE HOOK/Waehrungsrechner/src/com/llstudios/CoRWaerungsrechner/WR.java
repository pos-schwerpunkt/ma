package com.llstudios.CoRWaerungsrechner;

public abstract class WR implements IUmrechnen{
    protected WR nachfolger;
    public WR(WR nachfolger) {
        this.nachfolger = nachfolger;
    }

    public abstract boolean zustaendig(String variante);
    public abstract double berechnen(double betrag);

    @Override
    public double umrechnen(String variante, double betrag) throws WahrungGibsNicht {
        if (zustaendig(variante)){
            return berechnen(betrag);
        }
        else {
            if (this.nachfolger!=null){
                System.out.println("Ist nicht zuständig");
                return this.nachfolger.umrechnen(variante,betrag);
            }
            else {
                throw new WahrungGibsNicht();
            }

        }
    }
}
