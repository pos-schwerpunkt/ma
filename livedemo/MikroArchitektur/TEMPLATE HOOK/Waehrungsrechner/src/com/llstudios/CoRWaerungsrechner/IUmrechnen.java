package com.llstudios.CoRWaerungsrechner;

public interface IUmrechnen {
    double umrechnen(String variante, double betrag) throws WahrungGibsNicht;
}
