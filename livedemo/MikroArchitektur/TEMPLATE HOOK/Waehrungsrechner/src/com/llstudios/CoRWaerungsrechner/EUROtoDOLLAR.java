package com.llstudios.CoRWaerungsrechner;

public class EUROtoDOLLAR extends WR{
    public EUROtoDOLLAR(WR nachfolger) {
        super(nachfolger);
    }

    @Override
    public boolean zustaendig(String variante) {
        return variante.equals("EURtoUSD");
    }

    @Override
    public double berechnen(double betrag) {
        return betrag*0.83;
    }

}

