package com.llstudios.CoRWaerungsrechner;

public class EUROtoYEN extends WR {


    public EUROtoYEN(WR nachfolger) {
        super(nachfolger);
    }

    @Override
    public boolean zustaendig(String variante) {
        return variante.equals("EURtoYEN");
    }

    @Override
    public double berechnen(double betrag) {
        return betrag*100;
    }


}
