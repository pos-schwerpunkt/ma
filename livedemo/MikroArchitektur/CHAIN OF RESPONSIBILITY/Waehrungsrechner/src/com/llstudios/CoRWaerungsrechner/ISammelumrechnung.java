package com.llstudios.CoRWaerungsrechner;

public interface ISammelumrechnung {
    double sammelumrechnen(double[] betraege, String variante);
}
