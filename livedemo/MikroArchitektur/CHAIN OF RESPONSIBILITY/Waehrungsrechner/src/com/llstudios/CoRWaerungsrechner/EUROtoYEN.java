package com.llstudios.CoRWaerungsrechner;

public class EUROtoYEN extends WR {


    public EUROtoYEN(WR nachfolger) {
        super(nachfolger);
    }

    @Override
    public double umrechnen(String variante, double betrag) throws WahrungGibsNicht {
        if (variante.equals("EURtoYEN")){
            return betrag * 1000;
        }
        else {
            if(this.nachfolger!=null){
                return this.nachfolger.umrechnen(variante,betrag);
            }
            else{
                throw new WahrungGibsNicht();
            }

        }

    }
}
