package com.llstudios.CoRWaerungsrechner;

public class EUROtoDOLLAR extends WR{
    public EUROtoDOLLAR(WR nachfolger) {
        super(nachfolger);
    }

    @Override
    public double umrechnen(String variante, double betrag) throws WahrungGibsNicht {
        if (variante.equals("EURtoDollar")){
            return betrag * 0.93;
        }
        else {
            if(this.nachfolger!=null){
                return this.nachfolger.umrechnen(variante,betrag);
            }
            else{
                throw new WahrungGibsNicht();
            }

        }

    }
}
