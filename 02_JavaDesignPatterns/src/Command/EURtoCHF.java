package Command;

public class EURtoCHF implements IUmrechnen {
    private Command umrechner;
    private double betrag;
    String currency;

    public EURtoCHF(Command umrechner, String currency, double betrag) {
        this.umrechner = umrechner;
        this.currency = currency;
        this.betrag = betrag;
    }

    @Override
    public double umrechnen(double betrag) {
        return betrag * umrechner.getKurs();
    }

    @Override
    public double getBetrag() {
        return betrag;
    }

    @Override
    public String getCurrency() {
        return currency;
    }

    @Override
    public String toString() {
        return "EURtoCHF: " + "Betrag=" + betrag + ", Currency=" + currency;
    }
}
