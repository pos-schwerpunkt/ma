package Command;


public class EURtoDOLLAR implements IUmrechnen {
    private Command umrechner;
    private double betrag;
    String currency;

    public EURtoDOLLAR(Command umrechner, String currency, double betrag) {
        this.umrechner = umrechner;
        this.betrag = betrag;
        this.currency = currency;
    }

    @Override
    public double umrechnen(double betrag) {
        return betrag * umrechner.getKurs();
    }

    @Override
    public double getBetrag() {
        return betrag;
    }

    @Override
    public String getCurrency() {
        return currency;
    }

    @Override
    public String toString() {
        return "EURtoDOLLAR: " + "Betrag=" + betrag + ", Currency=" + currency;
    }
}

