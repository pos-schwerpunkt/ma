package Command;

public class App {
    public static void main(String[] args) {
        Command ch = new Command(1.022);
        Command us = new Command(1.234);
        EURtoCHF chf = new EURtoCHF(ch, "CHF", 1000);
        EURtoCHF chf2 = new EURtoCHF(ch, "CHF", 2000);
        Command umrechner = new Command();
        EURtoDOLLAR dollar = new EURtoDOLLAR(us, "Dollar", 1000);
        EURtoDOLLAR dollar2 = new EURtoDOLLAR(us, "Dollar", 2000);
        umrechner.addCalc(chf);
        umrechner.addCalc(chf2);
        umrechner.addCalc(dollar);
        umrechner.addCalc(dollar2);

        umrechner.execute();

        System.out.println("History:");
        umrechner.showHistory();

        System.out.println("Recalculating the second entry:");
        umrechner.reCalc(2);

        System.out.println("History:");
        umrechner.showHistory();

        System.out.println("Recalculation last entry and deleting it afterwards:");
        umrechner.calcAndDelete();

        System.out.println("History:");
        umrechner.showHistory();

        umrechner.replayDESC();
    }
}
