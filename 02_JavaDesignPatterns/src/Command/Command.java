package Command;

import java.util.ArrayList;

public class Command {
    private double kurs;
    private ArrayList<IUmrechnen> list = new ArrayList<>();

    public Command (double kurs) {
        this.kurs = kurs;
    }

    public Command () {
    }

    public double getKurs() {
        return kurs;
    }

    public void addCalc(IUmrechnen i) {
        list.add(i);
    }

    public void execute() {
        for (IUmrechnen i : list) {
            System.out.println(i.getBetrag() + " Euro sind " + i.umrechnen(i.getBetrag()) + " " + i.getCurrency());
            //return i.umrechnen(i.getBetrag());
        }
        //return i.umrechnen(i.getBetrag());
    }

    public void showHistory() {
        for (int i = 0; i < list.size(); i++) {
            System.out.println(i+1 + ".: " + list.get(i));
        }
    }

    public void reCalc(int i) {
        if (i < list.size()) {
            IUmrechnen u = list.get(i);
            System.out.println(u.umrechnen(u.getBetrag()));
            list.add(u);
        }
    }

    public void calcAndDelete() {
        int i = list.size() - 1;
        IUmrechnen u = list.get(i);
        u.umrechnen(u.getBetrag());
        list.remove(i);
    }

    public void replayASC() { // 1, 2, 3, ...
        execute();
    }

    public void replayDESC() { // ..., 4, 3, 2, 1
        IUmrechnen i;
        for (int j = list.size(); j > 0; j--) {
            i = list.get(j-1);
            System.out.println(i.getBetrag() + " Euro sind " + i.umrechnen(i.getBetrag()) + " " + i.getCurrency());
        }
    }
}
