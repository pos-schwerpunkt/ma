package CoF;

import basics.IUmrechnen;

public abstract class WR implements IUmrechnen {
    protected WR nachfolger;

    public WR(WR nachfolger) {
        this.nachfolger = nachfolger;
    }
}
