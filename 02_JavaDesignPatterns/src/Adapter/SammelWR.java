package Adapter;

import basics.CurrencyNotFoundException;
import basics.ISammelumrechnung;

import java.util.ArrayList;

public class SammelWR implements ISammelumrechnung {

    private ArrayList<Double> results = new ArrayList<>();

    @Override
    public double sammelUmrechnen(String var, double[] betrag) throws CurrencyNotFoundException {
        if(var.equals("EURtoCHF")) {
            for (double x : betrag) {
                x*=1.027;
                results.add(x);
            }
        } else if(var.equals("EURtoDOLLAR")) {
            for (double x : betrag) {
                x*=1.230;
                results.add(x);
            }
        } else {
            System.out.println("Unknown currency!");
            return 0.0;
        }
        double sum = 0.0;
        for (double x : results) {
            sum+=x;
        }
        return sum;
    }
}
