package Adapter;

import basics.CurrencyNotFoundException;

public class App {
    public static void main(String[] args) throws CurrencyNotFoundException {
        SingleWR singleWR = new SingleWR();
        SammelWR sammelWR = new SammelWR();
        double[] amounts = {100.0, 100.0};
        System.out.println("single-wr: " + singleWR.umrechnen("EURtoDOLLAR", 100));
        System.out.println("sammel-wr: " + sammelWR.sammelUmrechnen("EURtoDOLLAR", amounts));

        SammelWR adapter = new SingleCalcAdapter(singleWR);
        System.out.println("Adapter for sammel-wr: " + adapter.sammelUmrechnen("EURtoDOLLAR", amounts));
    }
}
