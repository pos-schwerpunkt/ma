package Adapter;

import basics.CurrencyNotFoundException;
import basics.IUmrechnen;

public class SingleWR implements IUmrechnen {
    @Override
    public double umrechnen(String var, double betrag) throws CurrencyNotFoundException {
        if(var.equals("EURtoCHF")) {
            return betrag*1.027;
        } else if(var.equals("EURtoDOLLAR")){
            return betrag*1.230;
        } else {
            System.out.println("Unknown currency!");
            return 0.0;
        }
    }
}
