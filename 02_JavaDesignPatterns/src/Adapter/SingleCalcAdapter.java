package Adapter;

import basics.CurrencyNotFoundException;
import basics.ISammelumrechnung;
import basics.IUmrechnen;

public class SingleCalcAdapter extends SammelWR implements ISammelumrechnung {
    IUmrechnen umrechnen;

    public SingleCalcAdapter(IUmrechnen umrechnen) {
        this.umrechnen = umrechnen;
    }

    @Override
    public double sammelUmrechnen(String var, double[] betrag) throws CurrencyNotFoundException {
        for (double x : betrag) {
            return umrechnen.umrechnen(var, x);
        }
        return 0.0;
    }
}
