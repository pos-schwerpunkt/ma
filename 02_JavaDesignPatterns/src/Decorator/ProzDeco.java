package Decorator;

import basics.CurrencyNotFoundException;

public class ProzDeco extends WRDecorator {
    public ProzDeco(WR nachfolger) {
        super(nachfolger);
    }

    @Override
    public double umrechnen(String var, double betrag) throws CurrencyNotFoundException {
        return nachfolger.umrechnen(var, betrag)*1.2;
    }
}
