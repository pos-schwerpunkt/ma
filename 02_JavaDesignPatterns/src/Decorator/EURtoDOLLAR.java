package Decorator;
import basics.CurrencyNotFoundException;

public class EURtoDOLLAR extends WR {
    public EURtoDOLLAR(WR nachfolger) {
        super(nachfolger);
    }

    @Override
    public double umrechnen(String var, double betrag) throws CurrencyNotFoundException {
        if(var.equals("EURtoDOLLAR")) {
            return betrag*0.95;
        } else {
            if(this.nachfolger != null) {
                System.out.println("Not responsible -> moving on to next chain member");
                return this.nachfolger.umrechnen(var, betrag);
            } else {
                throw new CurrencyNotFoundException();
            }
        }
    }
}
