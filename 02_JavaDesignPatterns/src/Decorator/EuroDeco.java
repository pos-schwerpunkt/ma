package Decorator;

import basics.CurrencyNotFoundException;

public class EuroDeco extends WRDecorator {

    public EuroDeco(WR nachfolger) {
        super(nachfolger);
    }

    @Override
    public double umrechnen(String var, double betrag) throws CurrencyNotFoundException {
        return nachfolger.umrechnen(var, betrag)+5;
    }
}
