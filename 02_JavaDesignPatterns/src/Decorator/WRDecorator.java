package Decorator;

import basics.CurrencyNotFoundException;

public abstract class WRDecorator extends WR {

    //protected WR wr;

    public WRDecorator(WR nachfolger) {
        super(nachfolger);
        //this.wr = wr;
    }

    @Override
    public double umrechnen(String var, double betrag) throws CurrencyNotFoundException {
        return nachfolger.umrechnen(var, betrag);
    }


}
