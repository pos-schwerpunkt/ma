package Decorator;

import basics.CurrencyNotFoundException;

public class EURtoYEN extends WR {
    public EURtoYEN(WR nachfolger) {
        super(nachfolger);
    }

    @Override
    public double umrechnen(String var, double betrag) throws CurrencyNotFoundException {
        if(var.equals("EURtoYEN")) {
            return betrag*10000;
        } else {
            if(this.nachfolger != null) {
                System.out.println("Not responsible -> moving on to next chain member");
                return this.nachfolger.umrechnen(var, betrag);
            } else {
                throw new CurrencyNotFoundException();
            }
        }
    }
}
