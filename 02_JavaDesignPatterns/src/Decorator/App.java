package Decorator;

import basics.CurrencyNotFoundException;

public class App {
    public static void main(String[] args) throws CurrencyNotFoundException {
//        WR kette = new EURtoDOLLAR(
//                new EuroDeco(
//                        new EURtoYEN(null), new EURtoYEN(new ProzDeco(
//                                new EURtoCHF(null), new EURtoCHF(null)
//                ))));
        WR kette = new EURtoDOLLAR(new EuroDeco(new EURtoCHF(new ProzDeco(new EURtoYEN(null)))));
        System.out.println("Decorated with +5EUR calc: " + kette.umrechnen("EURtoYEN", 100));
        System.out.println("Decorated with 20% fee: " + kette.umrechnen("EURtoCHF", 100));
    }
}
