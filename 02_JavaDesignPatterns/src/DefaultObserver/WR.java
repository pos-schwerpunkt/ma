package DefaultObserver;

import java.util.ArrayList;
import java.util.List;

public class WR {
    private final List<WRObserver> observers;

    public WR() {
        observers = new ArrayList<>();
    }

    public void subscribe(WRObserver observer) {
        observers.add(observer);
    }

    public void umrechnen(CURRENCY var, double betrag) {
        notifyObserver(var, betrag);
    }

    private void notifyObserver(CURRENCY var, double b) {
        for (var x : observers) {
            x.update(var, b);
        }
    }
}
