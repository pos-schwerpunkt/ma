package DefaultObserver;

public class EURtoCHF implements WRObserver {

    @Override
    public void update(CURRENCY var, double betrag) {
        System.out.println("Euro zu Franken umrechnen ...");
        if(var.equals(CURRENCY.CHF)) {
            System.out.println("Betrag in Franken: " + betrag*1.027);
        } else {
            System.out.println("Format nicht gültig.");
        }
    }
}
