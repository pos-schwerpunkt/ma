package DefaultObserver;

public class EURtoYEN implements WRObserver {

    @Override
    public void update(CURRENCY var, double betrag) {
        System.out.println("Euro zu Yen umrechnen ...");
        if(var.equals(CURRENCY.YEN)) {
            System.out.println("Betrag in Yen: " + betrag*1000.82);
        } else {
            System.out.println("Format nicht gültig.");
        }
    }
}
