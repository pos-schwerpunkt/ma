package DefaultObserver;

public class EURtoDOLLAR implements WRObserver {

    @Override
    public void update(CURRENCY var, double betrag) {
        System.out.println("Euro zu Dollar umgerechnet.");
        if(var.equals(CURRENCY.DOLLAR)) {
            System.out.println(betrag*1.220);
        } else {
            System.out.println("Format nicht gültig.");
        }
    }
}
