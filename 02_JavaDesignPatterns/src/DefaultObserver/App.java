package DefaultObserver;

public class App {
    public static void main(String[] args) {
        WR wr = new WR();
        wr.subscribe(new EURtoCHF());
        wr.subscribe(new EURtoDOLLAR());
        wr.subscribe(new EURtoYEN());
        wr.umrechnen(CURRENCY.DOLLAR, 100);
    }
}
