package Wither;

import Command.IUmrechnen;
import java.util.ArrayList;

public class WitherCommand {
    private final String var;
    private final double betrag;
    private ArrayList<IUmrechnen> list = new ArrayList<>();

    public WitherCommand(String var, double betrag) {
        this.var = var;
        this.betrag = betrag;
    }

    public WitherCommand withVariante(String v) {
        return new WitherCommand(v, this.betrag);
    }

    public WitherCommand withBetrag(double b) {
        return new WitherCommand(this.var, b);
    }

    public void addCalc(IUmrechnen i) {
        list.add(i);
    }

    public void execute() {
        for (IUmrechnen i : list) {
            System.out.println(i.getBetrag() + " Euro sind " + i.umrechnen(i.getBetrag()) + " " + i.getCurrency());
        }
    }

    public String getVar() {
        return var;
    }

    public double getBetrag() {
        return betrag;
    }
}
