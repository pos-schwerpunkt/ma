package Wither;

public class App {
    public static void main(String[] args) {
        WitherCommand command = new WitherCommand("EURtoDOLLAR", 1000);
        System.out.println(command.getBetrag());
        WitherCommand command1 = command.withBetrag(500);
        System.out.println(command1.getBetrag());
        WitherCommand command2 = command.withVariante("EURtoCHF");
        System.out.println(command2.getVar());
    }
}
