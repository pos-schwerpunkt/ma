package TemplateHook;

public class EURtoCHF extends WR {
    public EURtoCHF(WR nachfolger) {
        super(nachfolger);
    }

    @Override
    public boolean responsible(String var) {
        return var.equals("EURtoCHF");
    }

    @Override
    public double berechnen(double betrag) {
        return betrag*1.07;
    }
}
