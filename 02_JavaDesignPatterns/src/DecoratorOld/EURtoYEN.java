package DecoratorOld;

public class EURtoYEN extends SimpleWR {
    public EURtoYEN(SimpleWR nachfolger) {
        super(nachfolger);
    }

    @Override
    public boolean responsible(String var) {
        return var.equals("EURtoYEN");
    }

    @Override
    public double berechnen(double betrag) {
        return betrag*10000;
    }


}
