package DecoratorOld;

import basics.IUmrechnen;

public interface WR extends IUmrechnen {
    //double umrechnen(String var, double betrag) throws CurrencyNotFoundException;
    boolean responsible(String var);
    double berechnen(double betrag);
}
