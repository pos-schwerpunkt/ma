package DecoratorOld;

import basics.CurrencyNotFoundException;

public abstract class SimpleWR implements WR {
    protected WR nachfolger;

    public SimpleWR(WR nachfolger) {
        this.nachfolger = nachfolger;
    }

    public abstract boolean responsible(String var);
    public abstract double berechnen(double betrag);

    @Override
    public double umrechnen(String var, double betrag) throws CurrencyNotFoundException {
        if(responsible(var)) {
            System.out.println("Not decorated, usual calc");
            return berechnen(betrag);
        } else {
            if(this.nachfolger != null) {
                System.out.println(getClass().getCanonicalName() + " is not responsible --> moving on to next chain");
                return this.nachfolger.umrechnen(var, betrag);
            } else {
                throw new CurrencyNotFoundException();
            }
        }
    }
}
