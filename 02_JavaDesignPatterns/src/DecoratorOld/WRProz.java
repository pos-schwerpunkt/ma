package DecoratorOld;

import basics.CurrencyNotFoundException;

public abstract class WRProz implements WR {

    protected WR nachfolger;

    public WRProz(WR nachfolger) {
        this.nachfolger = nachfolger;
    }

    public abstract boolean responsible(String var);
    public abstract double berechnen(double betrag);

    @Override
    public double umrechnen(String var, double betrag) throws CurrencyNotFoundException {
        if(responsible(var)) {
            System.out.println("Decorated!!");
            return berechnen(betrag)*1.005;
        } else {
            if(this.nachfolger != null) {
                System.out.println(getClass().getCanonicalName() + " is not responsible --> moving on to next chain");
                return this.nachfolger.umrechnen(var, betrag);
            } else {
                throw new CurrencyNotFoundException();
            }
        }
    }
}
