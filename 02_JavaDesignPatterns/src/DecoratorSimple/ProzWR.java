package DecoratorSimple;

import basics.CurrencyNotFoundException;
import basics.IUmrechnen;

public class ProzWR implements IUmrechnen {
    private final IUmrechnen decorated;

    public ProzWR(IUmrechnen decorated) {
        this.decorated = decorated;
    }

    @Override
    public double umrechnen(String var, double betrag) throws CurrencyNotFoundException {
        System.out.println("Decorator with 0,5%!");
        return decorated.umrechnen(var, (betrag*1.025))*1.005;
    }
}
