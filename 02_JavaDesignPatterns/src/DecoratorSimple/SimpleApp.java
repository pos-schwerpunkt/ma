package DecoratorSimple;

import basics.CurrencyNotFoundException;

public class SimpleApp {
    public static void main(String[] args) throws CurrencyNotFoundException {
        SimpleWR simpleWR = new SimpleWR();
        System.out.println(simpleWR.umrechnen("euro", 100));

        ProzWR prozWR = new ProzWR(simpleWR);
        System.out.println(prozWR.umrechnen("euro", 100));

        EurWR eurWR = new EurWR(simpleWR);
        System.out.println(eurWR.umrechnen("euro", 100));
    }
}
