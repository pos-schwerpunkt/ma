package DecoratorSimple;

import basics.CurrencyNotFoundException;
import basics.IUmrechnen;

public class SimpleWR implements IUmrechnen {

    @Override
    public double umrechnen(String var, double betrag) throws CurrencyNotFoundException {
        System.out.println("SimpleWR");
        return betrag*1.025;
    }
}
