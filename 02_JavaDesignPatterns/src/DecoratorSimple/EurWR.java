package DecoratorSimple;

import basics.CurrencyNotFoundException;
import basics.IUmrechnen;

public class EurWR implements IUmrechnen  {
    private final IUmrechnen decorated;

    public EurWR(IUmrechnen decorated) {
        this.decorated = decorated;
    }

    @Override
    public double umrechnen(String var, double betrag) throws CurrencyNotFoundException {
        System.out.println("Decorator with 5 EUR fee!");
        return decorated.umrechnen(var,betrag)*1.025+5;
    }
}
