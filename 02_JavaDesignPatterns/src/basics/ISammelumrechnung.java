package basics;

public interface ISammelumrechnung {
    double sammelUmrechnen(String var, double[] betrag) throws CurrencyNotFoundException;
}
