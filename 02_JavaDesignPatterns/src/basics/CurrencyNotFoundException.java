package basics;

public class CurrencyNotFoundException extends Exception {
    public CurrencyNotFoundException() {
        super("Währung konnte nicht umgerechnet werden.");
    }
}
