package Builder;

public class WRBuilder implements IWR {
    private CURRENCY currency;
    private double betrag;
    private double fee;

    @Override
    public WRBuilder setCurrency(CURRENCY currency) {
        this.currency = currency;
        return this;
    }

    @Override
    public WRBuilder setBetrag(double betrag) {
        this.betrag = betrag;
        return this;
    }

    public WRBuilder setFee(double fee) {
        this.fee = fee;
        return this;
    }

    @Override
    public double umrechnen() {
        if (currency.equals(CURRENCY.DOLLAR)) {
            return this.betrag * 1.220;
        } else if (currency.equals(CURRENCY.CHF)) {
            return this.betrag * 1.024;
        } else if (currency.equals(CURRENCY.YEN)) {
            return this.betrag * 100000;
        } else {
            return this.betrag;
        }
    }

    public WR getWR() {
        return new WR(currency, betrag, fee);
    }
}
