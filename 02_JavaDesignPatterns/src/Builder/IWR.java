package Builder;

public interface IWR {
    WRBuilder setCurrency(CURRENCY currency);
    WRBuilder setBetrag(double betrag);
    double umrechnen();
}
