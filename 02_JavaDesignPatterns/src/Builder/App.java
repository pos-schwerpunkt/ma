package Builder;

public class App {
    public static void main(String[] args) {
        WR wr1 = new WRBuilder().setCurrency(CURRENCY.EURO).setBetrag(1000).getWR();
        System.out.println(wr1);
        WR wr2 = new WRBuilder().setCurrency(CURRENCY.DOLLAR).setBetrag(1000).setFee(5).getWR();
        System.out.println(wr2);
    }
}
