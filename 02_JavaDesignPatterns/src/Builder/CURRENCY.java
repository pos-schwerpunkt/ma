package Builder;

public enum CURRENCY {
    EURO,
    DOLLAR,
    YEN,
    CHF
}
