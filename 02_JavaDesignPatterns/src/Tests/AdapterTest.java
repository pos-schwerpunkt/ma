package Tests;

import Adapter.SammelWR;
import Adapter.SingleCalcAdapter;
import Adapter.SingleWR;
import basics.CurrencyNotFoundException;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

public class AdapterTest {
    @Test
    public void testCalc() throws CurrencyNotFoundException {
        SingleWR singleWR = new SingleWR();
        SammelWR sammelWR = new SammelWR();
        double[] amounts = {100.0, 100.0};
        SammelWR adapter = new SingleCalcAdapter(singleWR);

        Assertions.assertEquals(123.0, singleWR.umrechnen("EURtoDOLLAR", 100),0.01);
        Assertions.assertEquals(246.0, sammelWR.sammelUmrechnen("EURtoDOLLAR", amounts), 0.01);
        Assertions.assertEquals(123.0, adapter.sammelUmrechnen("EURtoDOLLAR", amounts), 0.01);
    }
}
