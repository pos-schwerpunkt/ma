package Tests;

import Decorator.*;
import basics.CurrencyNotFoundException;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

public class DecoratorTest {
    @Test
    public void testCalc() throws CurrencyNotFoundException {
        WR kette = new EURtoDOLLAR(new EuroDeco(new EURtoCHF(new ProzDeco(new EURtoYEN(null)))));
        Assertions.assertEquals(1000005.0, kette.umrechnen("EURtoYEN", 100));
        Assertions.assertEquals(133.4, kette.umrechnen("EURtoCHF", 100));
    }
}
