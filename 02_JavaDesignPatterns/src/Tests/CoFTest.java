package Tests;

import CoF.EURtoCHF;
import CoF.EURtoDOLLAR;
import CoF.EURtoYEN;
import CoF.WR;
import basics.CurrencyNotFoundException;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CoFTest {
    @Test
    public void testCalc() throws CurrencyNotFoundException {
        WR wr = new EURtoDOLLAR(new EURtoYEN(new EURtoCHF(null)));
        double result = wr.umrechnen("EURtoCHF", 100);
        assertEquals(107.0, result, 0.001);
    }
}
