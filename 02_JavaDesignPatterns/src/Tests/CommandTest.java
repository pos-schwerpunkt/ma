package Tests;

import Command.Command;
import basics.CurrencyNotFoundException;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

public class CommandTest {
    @Test
    public void testCalc() throws CurrencyNotFoundException {
        Command ch = new Command(1.022);
        Command us = new Command(1.234);
        Assertions.assertEquals(1.022, ch.getKurs(),0.0);
        Assertions.assertEquals(1.234, us.getKurs(),0.0);
    }
}
